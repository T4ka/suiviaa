package com.ferraioli.suivaa.Manager;

import com.ferraioli.suivaa.ModelApi.Cabinet;
import com.ferraioli.suivaa.ModelApi.Medecin;
import com.ferraioli.suivaa.ModelApi.Visite;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Ferraioli on 26/04/2018.
 */

public interface ApiService {

    @GET("api/visite")
    Call<List<Visite>> getVisite();

    @POST("api/visite")
    Call<Visite> setVisite(@Body Visite visite);

    @GET("api/medecin")
    Call<List<Medecin>> getMedecin();

    @GET("api/medecin/{id}")
    Call<List<Medecin>>  getMedecinById(@Path("id") Integer id);

    @GET("api/cabinet")
    Call<List<Cabinet>> getCabinet();

    @GET("api/cabinet/{id}")
    Call<List<Cabinet>>  getCabinetById(@Path("id") Integer id);

}

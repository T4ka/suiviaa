package com.ferraioli.suivaa.Manager;

import com.ferraioli.suivaa.ModelApi.Medecin;
import com.ferraioli.suivaa.ModelApi.Visite;

import java.util.List;

/**
 * Created by Ferraioli on 30/04/2018.
 */

public interface ServiceInterface {
    void getMedecin(List<Medecin> result);
    void getVisite(List<Visite> result);
}

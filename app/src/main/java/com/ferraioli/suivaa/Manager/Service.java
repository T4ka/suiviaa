package com.ferraioli.suivaa.Manager;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.ferraioli.suivaa.ModelApi.Cabinet;
import com.ferraioli.suivaa.ModelApi.Medecin;
import com.ferraioli.suivaa.ModelApi.Visite;
import com.ferraioli.suivaa.Util.Form;
import com.ferraioli.suivaa.Util.GPSTracker;

import java.util.ArrayList;
import java.util.List;

import me.riddhimanadib.formmaster.FormBuilder;
import me.riddhimanadib.formmaster.model.FormElementPickerSingle;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ferraioli on 29/04/2018.
 */

public class Service{

    private static ApiService service = ServiceManager.createService(ApiService.class);
    public static List<Visite> notify =  new ArrayList<>();
    private static List<String> MedecinAndCabinet = new ArrayList<>();

    private Service(){
        service = ServiceManager.createService(ApiService.class);
    }

    public static void getMedecin(final FormBuilder mFormBuilder, final GPSTracker gps) {
        final Callback<List<Medecin>> serviceCallback = new Callback<List<Medecin>>() {
            @Override
            public void onResponse(Call<List<Medecin>> call, Response<List<Medecin>> response) {
                final List<String> MedecinEtCabinet = new ArrayList<>();
                if(response.isSuccessful()) {
                    FormElementPickerSingle medecin = FormElementPickerSingle.createInstance().setTitle("Médecin et cabinets").setOptions(MedecinEtCabinet).setPickerTitle("Choisissez votre médecin et cabinet").setHint("Votre médecin et cabinet..").setTag(7);
                    Form.formItems.add(medecin);
                    for(Medecin m : response.body()){
                        getCabinetId(mFormBuilder, m.getIdCabinet(), m, MedecinEtCabinet, gps, medecin);
                    }

                }


            }

            @Override
            public void onFailure(Call<List<Medecin>> call, Throwable t) {
                Log.e("SynchroManager", "Load Medias", t);
                Form.formItems.clear();
            }
        };

        ApiService service = ServiceManager.createService(ApiService.class);
        Call<List<Medecin>> call = service.getMedecin();
        call.enqueue(serviceCallback);
    }

    private static void getCabinetId(final FormBuilder mFormBuilder, int id, final Medecin m, final List<String> MedecinEtCabinet, final GPSTracker gps, final FormElementPickerSingle medecin) {
        Callback<List<Cabinet>> serviceCallback = new Callback<List<Cabinet>> () {
            @Override
            public void onResponse(Call<List<Cabinet>> call, Response<List<Cabinet>> response) {


                MedecinEtCabinet.add(m.getId() + " - " + m.getNom() + " - " + response.body().get(0).getVille() + ", " + response.body().get(0).getAdresse());
                mFormBuilder.addFormElements(Form.formItems);
            }

            @Override
            public void onFailure(Call<List<Cabinet>> call, Throwable t) {
                Log.e("SynchroManager", "Load Medias", t);
            }
        };

        ApiService service = ServiceManager.createService(ApiService.class);
        Call<List<Cabinet>>  call = service.getCabinetById(id);
        call.enqueue(serviceCallback);

    }

    public static void getCabinet(final FormBuilder mFormBuilder) {
        Callback<List<Cabinet>> serviceCallback = new Callback<List<Cabinet>>() {
            @Override
            public void onResponse(Call<List<Cabinet>> call, Response<List<Cabinet>> response) {
                final List<String> fruits = new ArrayList<>();
                for(Cabinet c : response.body()){
                    fruits.add(c.getID() + "-" + c.getVille());
                }
                FormElementPickerSingle cabinet = FormElementPickerSingle.createInstance().setTitle("Cabinet").setOptions(fruits).setPickerTitle("Choisissez votre cabinet").setHint("Votre cabinet..");
                Form.formItems.add(cabinet);
                mFormBuilder.addFormElements(Form.formItems);


            }

            @Override
            public void onFailure(Call<List<Cabinet>> call, Throwable t) {
                Log.e("SynchroManager", "Load Medias", t);
                Form.formItems.clear();
            }
        };

        ApiService service = ServiceManager.createService(ApiService.class);
        Call<List<Cabinet>> call = service.getCabinet();
        call.enqueue(serviceCallback);
    }

    public static void sendVisite(Visite visite, final Context context, final boolean toast) {
        Callback<Visite> serviceCallback = new Callback<Visite>() {
            @Override
            public void onResponse(Call<Visite> call, Response<Visite> response) {
                if(toast) {
                    Toast toast = Toast.makeText(context, "Visite ajoutée avec succès !", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

            }

            @Override
            public void onFailure(Call<Visite> call, Throwable t) {
                Log.e("SynchroManager", "Load Medias", t);
            }
        };

        ApiService service = ServiceManager.createService(ApiService.class);
        Call<Visite> call = service.setVisite(visite);
        call.enqueue(serviceCallback);
    }





}

package com.ferraioli.suivaa.Provider;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.ferraioli.suivaa.ModelBDD.VisiteLocal;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by mlagast on 28/10/2017.
 */

@Dao
public interface VisiteInterface {


    @Insert(onConflict = REPLACE)
    void addVisiteBdd(VisiteLocal visites);

    @Query("UPDATE VisiteLocal SET est_en_api = :api")
    void updateVisiteBdd(boolean api);

    @Query("SELECT * FROM VisiteLocal")
    LiveData<List<VisiteLocal>> getAll();

    @Query("SELECT * FROM VisiteLocal WHERE est_en_api = :api")
    LiveData<List<VisiteLocal>> getAllModify(boolean api);
}
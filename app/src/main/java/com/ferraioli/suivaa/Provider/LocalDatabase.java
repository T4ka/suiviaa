package com.ferraioli.suivaa.Provider;

/**
 * Created by Ferraioli on 25/04/2018.
 */

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.ferraioli.suivaa.ModelBDD.VisiteLocal;


@Database(entities = {VisiteLocal.class}, version = 13)
public abstract class LocalDatabase extends RoomDatabase {
    private static LocalDatabase INSTANCE;

    public static LocalDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), LocalDatabase.class, "todo_db")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }

    public abstract VisiteInterface taskProvider();

}
package com.ferraioli.suivaa.Activity;

/**
 * Created by Ferraioli on 26/04/2018.
 */

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ferraioli.suivaa.Adapter.VisiteAdapterApi;
import com.ferraioli.suivaa.Adapter.VisiteAdapterBdd;
import com.ferraioli.suivaa.Manager.ApiService;
import com.ferraioli.suivaa.Manager.Service;
import com.ferraioli.suivaa.Manager.ServiceManager;
import com.ferraioli.suivaa.ModelApi.Visite;
import com.ferraioli.suivaa.ModelBDD.VisiteLocal;
import com.ferraioli.suivaa.Provider.LocalDatabase;
import com.ferraioli.suivaa.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisitesActivity extends AppCompatActivity {

    private TextView mTextMessage;
    //private MyListAdapter adapter;
    private VisiteAdapterBdd adapter;
    private VisiteAdapterApi etatAdapter;
    protected ListView listView;
    protected EditText editText;
    List<Visite> objects = new ArrayList<>();

    private LocalDatabase database;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    finish();
                    return true;
                case R.id.navigation_notifications:
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.visite_activity);

        listView = (ListView) findViewById(R.id.visite_list_id);
        adapter = new VisiteAdapterBdd(this, new ArrayList<VisiteLocal>());
        if(isNetworkAvailable()){
            etatAdapter = new VisiteAdapterApi(this, new ArrayList<Visite>());
        } else {
            etatAdapter = new VisiteAdapterApi(this, new ArrayList<VisiteLocal>());
        }

            listView.setAdapter(etatAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String date = ((TextView)view.findViewById(R.id.date)).getText().toString().split("T")[0];
                String rdv = ((TextView)view.findViewById(R.id.rdv)).getText().toString();
                String final_rdv = rdv == "1" ? "oui" : "non";
                String heure_arrive = ((TextView)view.findViewById(R.id.heurearrive)).getText().toString();
                String heure_depart = ((TextView)view.findViewById(R.id.heuredepart)).getText().toString();
                String medecin = ((TextView)view.findViewById(R.id.medecin)).getText().toString();
                String cabinet = ((TextView)view.findViewById(R.id.cabinet)).getText().toString();


                Toast.makeText(getApplicationContext(),
                        "Date : " + date +"\n"
                                +"Rendez-vous : " + final_rdv +"\n"
                                +"Heure arrivé : " +heure_arrive +"\n"
                                +"Heure départ : " +heure_depart +"\n"
                                +"Médecin : " +medecin +"\n"
                                +"Cabinet : " +cabinet, Toast.LENGTH_SHORT).show();
            }
        });






        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        database = LocalDatabase.getDatabase(this.getApplication());
        if(isNetworkAvailable()) {
            loadData();
            LiveData<List<VisiteLocal>> bdd_en_api = database.taskProvider().getAllModify(false);
            bdd_en_api.observe(this, new Observer<List<VisiteLocal>>() {
                @Override
                public void onChanged(@Nullable List<VisiteLocal> bdd_en_api) {
                    boolean update = false;
                    for(VisiteLocal v : bdd_en_api){
                        Visite visite = new Visite();
                        visite.setIdMedecin(v.id_medecin);
                        visite.setRdv("oui");
                        visite.setHeureDepart(v.heure_depart);
                        visite.setHeureArrive(v.heure_arrive);
                        visite.setDate(v.date);
                        Service.sendVisite(visite, getApplicationContext(), false);
                        update = true;
                    }

                    if(update){
                        new addAsyncTask(database, getApplicationContext()).execute();
                    }




                }

            });
        } else {
            LiveData<List<VisiteLocal>> visites = database.taskProvider().getAll();
            visites.observe(this, new Observer<List<VisiteLocal>>() {

                @Override
                public void onChanged(@Nullable List<VisiteLocal> visites) {
                    etatAdapter.clear();
                    etatAdapter.addAll(visites);
                    etatAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    /* Async task to update database */
    private static class addAsyncTask extends AsyncTask<Void, Void, Void> {
        private LocalDatabase db;
        private Context context;

        addAsyncTask(LocalDatabase appDatabase, Context ctx) {
            db = appDatabase;
            context = ctx;
        }

        @Override
        protected Void doInBackground(Void... params) {
            db.taskProvider().updateVisiteBdd(true);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

                Toast toast = Toast.makeText(context, "Ajout des visites local vers webservice..", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

        }

    }

    public void loadData() {
        Callback<List<Visite>> serviceCallback = new Callback<List<Visite>>() {
            @Override
            public void onResponse(Call<List<Visite>> call, final Response<List<Visite>> response) {
                final List<Visite> objects = response.body();
                etatAdapter.clear();
                etatAdapter.addAll(objects);
                etatAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<List<Visite>> call, Throwable t) {
                Log.e("SynchroManager", "Load Medias", t);
            }
        };

        ApiService service = ServiceManager.createService(ApiService.class);
        Call<List<Visite>> call = service.getVisite();
        call.enqueue(serviceCallback);
    }



    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}

package com.ferraioli.suivaa;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ferraioli.suivaa.Activity.VisitesActivity;
import com.ferraioli.suivaa.Adapter.VisiteAdapterApi;
import com.ferraioli.suivaa.Adapter.VisiteAdapterBdd;
import com.ferraioli.suivaa.Manager.Service;
import com.ferraioli.suivaa.ModelApi.Medecin;
import com.ferraioli.suivaa.ModelApi.Visite;
import com.ferraioli.suivaa.ModelBDD.VisiteLocal;
import com.ferraioli.suivaa.Provider.LocalDatabase;
import com.ferraioli.suivaa.Util.Form;
import com.ferraioli.suivaa.Util.GPSTracker;

import java.util.ArrayList;
import java.util.List;

import me.riddhimanadib.formmaster.FormBuilder;
import me.riddhimanadib.formmaster.model.BaseFormElement;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;
    //private MyListAdapter adapter;
    private VisiteAdapterBdd adapter;
    private VisiteAdapterApi etatAdapter;
    protected ListView listView;
    private RecyclerView mRecyclerView;
    private FormBuilder mFormBuilder;
    private List<Medecin> medecins = new ArrayList<>();
    List<BaseFormElement> formItems = new ArrayList<>();
    protected ListView etatView;
    protected EditText editText;
    GPSTracker gps;


    private static final int TAG_NOM = 1;
    private static final int TAG_PRENOM = 2;
    private static final int TAG_DATE = 3;
    private static final int TAG_RDV = 4;
    private static final int TAG_HEUREA = 5;
    private static final int TAG_HEURED = 6;
    private static final int TAG_MEDECIN = 7;
    private static final int TAG_CABINET = 8;



    private LocalDatabase database;
    public LiveData<List<VisiteLocal>> visites;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    return true;
                case R.id.navigation_notifications:
                   // mTextMessage.setText(R.string.title_notifications);
                    Intent intent = new Intent(MainActivity.this, VisitesActivity.class);
                    startActivity(intent);
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the

                    // contacts-related task you need to do.

                    gps = new GPSTracker(this, MainActivity.this);

                    // Check if GPS enabled
                    if (gps.canGetLocation()) {

                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();

                        // \n is for new line
                        Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                    } else {
                        // Can't get location.
                        // GPS or network is not enabled.
                        // Ask user to enable GPS/network in settings.
                        gps.showSettingsAlert();
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                    Toast.makeText(this, "You need to grant permission", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mFormBuilder = new FormBuilder(this, mRecyclerView);
        Button button = (Button) findViewById(R.id.button4);
        gps = new GPSTracker(this, MainActivity.this);
        // Check if GPS enabled
        if (!gps.canGetLocation()) {
            gps.showSettingsAlert();
        }




        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mFormBuilder.isValidForm()){
                    Visite v = new Visite();
                    v.setDate(mFormBuilder.getFormElement(Form.TAG_DATE).getValue());
                    v.setHeureArrive(mFormBuilder.getFormElement(Form.TAG_HEUREA).getValue());
                    v.setHeureDepart(mFormBuilder.getFormElement(Form.TAG_HEURED).getValue());
                    v.setRdv(mFormBuilder.getFormElement(Form.TAG_RDV).getValue());

                    if(isNetworkAvailable()) {
                        v.setIdMedecin(Integer.parseInt(mFormBuilder.getFormElement(7).getValue().split("-")[0].trim()));
                        Service.sendVisite(v, getApplicationContext(), true);
                    } else {
                        addVisiteBdd(v.getDate(), v.getHeureArrive(), v.getHeureDepart(), v.getRdv(), 1);
                        Service.notify.add(v);
                    }
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(),"Veuillez valider tous les champs !", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });


        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        database = LocalDatabase.getDatabase(this.getApplication());
        if(isNetworkAvailable()) {
            Service.getMedecin(mFormBuilder, gps);
        }
        Log.d("tamerere", String.valueOf(isNetworkAvailable()));
        Form.formItems.clear();
        Form.setupForm();
        mFormBuilder.addFormElements(Form.formItems);

    }

    public void addVisiteBdd(String date, String heure_arrive, String heure_depart, String rdv, Integer id_medecin) {
        VisiteLocal visites = new VisiteLocal();
        visites.date = date;
        visites.heure_arrive = heure_arrive;
        visites.heure_depart = heure_depart;
        visites.rdv = rdv;
        visites.id_medecin = id_medecin;
        new addAsyncTask(database, getApplicationContext()).execute(visites);
    }

    /* Async task to update database */
    private static class addAsyncTask extends AsyncTask<VisiteLocal, Void, Boolean> {
        private LocalDatabase db;
        private Context context;

        addAsyncTask(LocalDatabase appDatabase, Context ctx) {
            db = appDatabase;
            context = ctx;
        }

        @Override
        protected Boolean doInBackground(final VisiteLocal... params) {
            db.taskProvider().addVisiteBdd(params[0]);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result){
                Toast toast = Toast.makeText(context,"Visite ajouté avec succès !", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }

    }





    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }



}

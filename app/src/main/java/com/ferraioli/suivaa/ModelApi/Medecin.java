package com.ferraioli.suivaa.ModelApi;

/**
 * Created by Ferraioli on 29/04/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Medecin {

    @SerializedName("idMedecin")
    @Expose
    private Integer id;
    @SerializedName("nom")
    @Expose
    private String nom;
    @SerializedName("prenom")
    @Expose
    private String prenom;
    @SerializedName("idcabinet")
    @Expose
    private Integer idCabinet;

    /**
     * No args constructor for use in serialization
     *
     */
    public Medecin() {
    }

    /**
     *
     * @param prenom
     * @param id
     * @param idCabinet
     * @param nom
     */
    public Medecin(Integer id, String nom, String prenom, Integer idCabinet) {
        super();
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.idCabinet = idCabinet;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Integer getIdCabinet() {
        return idCabinet;
    }

    public void setIdCabinet(Integer idCabinet) {
        this.idCabinet = idCabinet;
    }

}
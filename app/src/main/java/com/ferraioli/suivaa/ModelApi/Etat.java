package com.ferraioli.suivaa.ModelApi;

/**
 * Created by Ferraioli on 26/04/2018.
 */

public class Etat {

    private String id;
    private String libelle;

    /**
     * No args constructor for use in serialization
     *
     */
    public Etat() {
    }

    /**
     *
     * @param id
     * @param libelle
     */
    public Etat(String id, String libelle) {
        super();
        this.id = id;
        this.libelle = libelle;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

}
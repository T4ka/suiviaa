package com.ferraioli.suivaa.ModelApi;

/**
 * Created by Ferraioli on 30/04/2018.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cabinet {

    @SerializedName("idCabinet")
    @Expose
    private Integer iD;
    @SerializedName("ville")
    @Expose
    private String ville;
    @SerializedName("adresse")
    @Expose
    private String adresse;
    @SerializedName("gps")
    @Expose
    private String gps;

    /**
     * No args constructor for use in serialization
     *
     */
    public Cabinet() {
    }

    /**
     *
     * @param adresse
     * @param gps
     * @param ville
     * @param iD
     */
    public Cabinet(Integer iD, String ville, String adresse, String gps) {
        super();
        this.iD = iD;
        this.ville = ville;
        this.adresse = adresse;
        this.gps = gps;
    }

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getGps() {
        return gps;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }

}
package com.ferraioli.suivaa.ModelApi;

/**
 * Created by Ferraioli on 26/04/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Visite {

    @SerializedName("idVisite")
    @Expose
    private Integer id;
    @SerializedName("dateV")
    @Expose
    private String date;
    @SerializedName("rdv")
    @Expose
    private String rdv;
    @SerializedName("heure_arrive")
    @Expose
    private String heureArrive;
    @SerializedName("heure_depart")
    @Expose
    private String heureDepart;
    @SerializedName("idmedecin")
    @Expose
    private Integer idMedecin;

    /**
     * No args constructor for use in serialization
     */
    public Visite() {
    }

    /**
     * @param heureArrive
     * @param id
     * @param idMedecin
     * @param rdv
     * @param heureDepart
     * @param date
     */
    public Visite(Integer id, String date, String rdv, String heureArrive, String heureDepart, Integer idMedecin) {
        super();
        this.id = id;
        this.date = date;
        this.rdv = rdv;
        this.heureArrive = heureArrive;
        this.heureDepart = heureDepart;
        this.idMedecin = idMedecin;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRdv() {
        return rdv;
    }

    public void setRdv(String rdv) {
        this.rdv = rdv;
    }

    public String getHeureArrive() {
        return heureArrive;
    }

    public void setHeureArrive(String heureArrive) {
        this.heureArrive = heureArrive;
    }

    public String getHeureDepart() {
        return heureDepart;
    }

    public void setHeureDepart(String heureDepart) {
        this.heureDepart = heureDepart;
    }

    public Integer getIdMedecin() {
        return idMedecin;
    }

    public void setIdMedecin(Integer idMedecin) {
        this.idMedecin = idMedecin;
    }
}
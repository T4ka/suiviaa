package com.ferraioli.suivaa.Util;

import java.util.ArrayList;
import java.util.List;

import me.riddhimanadib.formmaster.model.BaseFormElement;
import me.riddhimanadib.formmaster.model.FormElementPickerDate;
import me.riddhimanadib.formmaster.model.FormElementPickerTime;
import me.riddhimanadib.formmaster.model.FormElementTextNumber;
import me.riddhimanadib.formmaster.model.FormElementTextSingleLine;
import me.riddhimanadib.formmaster.model.FormHeader;

/**
 * Created by Ferraioli on 30/04/2018.
 */

public class Form {
    public static final List<BaseFormElement> formItems = new ArrayList<>();
    public static final FormHeader header = FormHeader.createInstance("Ajouter une visite");
    public static final int TAG_NOM = 1;
    public static final int TAG_PRENOM = 2;
    public static final int TAG_DATE = 3;
    public static final int TAG_RDV = 4;
    public static final int TAG_HEUREA = 5;
    public static final int TAG_HEURED = 6;
    public static final int TAG_MEDECIN = 7;
    public static final int TAG_CABINET = 8;


    public static void setupForm(){
        FormElementTextSingleLine nom = FormElementTextSingleLine .createInstance().setTag(TAG_NOM).setTitle("Nom").setHint("Votre nom..").setRequired(true);
        FormElementTextSingleLine prenom = FormElementTextSingleLine.createInstance().setTag(TAG_PRENOM).setTitle("Prénom").setHint("Votre prénom..").setRequired(true);
        FormElementPickerDate date = FormElementPickerDate.createInstance().setTag(TAG_DATE).setTitle("Date").setHint("Votre date..").setRequired(true);
        FormElementTextNumber rdv = FormElementTextNumber.createInstance().setTag(TAG_RDV).setTitle("Rendez-vous").setHint("0 = non, 1 = oui..").setRequired(true);
        FormElementPickerTime heure_arrive = FormElementPickerTime.createInstance().setTag(TAG_HEUREA).setTitle("Heure arrivé").setHint("Votre heure d'arrivé..").setRequired(true);
        FormElementPickerTime heure_deapart = FormElementPickerTime.createInstance().setTag(TAG_HEURED).setTitle("Heure départ").setHint("Votre heure de départ..").setRequired(true);
        formItems.add(header);
        formItems.add(nom);
        formItems.add(prenom);
        formItems.add(date);
        formItems.add(rdv);
        formItems.add(heure_arrive);
        formItems.add(heure_deapart);

    }
}

package com.ferraioli.suivaa.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ferraioli.suivaa.Manager.ApiService;
import com.ferraioli.suivaa.Manager.ServiceManager;
import com.ferraioli.suivaa.ModelApi.Cabinet;
import com.ferraioli.suivaa.ModelApi.Medecin;
import com.ferraioli.suivaa.ModelApi.Visite;
import com.ferraioli.suivaa.ModelBDD.VisiteLocal;
import com.ferraioli.suivaa.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ferraioli on 26/04/2018.
 */


public class VisiteAdapterApi<T> extends ArrayAdapter<T> {
    private final Context context;
    private final ArrayList<T> values;
    private ArrayList<Visite> visite;
    private ArrayList<VisiteLocal> visite_bdd;

    public VisiteAdapterApi(Context context, ArrayList<T> values) {
        super(context, R.layout.etat_list, values);
        this.context = context;
        this.values = values;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.etat_list, parent, false);
        }

        TextView date = (TextView) convertView.findViewById(R.id.date);
        TextView rdv = (TextView) convertView.findViewById(R.id.rdv);
        TextView heurearrive = (TextView) convertView.findViewById(R.id.heurearrive);
        TextView heuredepart = (TextView) convertView.findViewById(R.id.heuredepart);
        TextView medecin = (TextView) convertView.findViewById(R.id.medecin);
        TextView cabinet = (TextView) convertView.findViewById(R.id.cabinet);

        if(values.get(position) instanceof  Visite){
            visite=(ArrayList<Visite>) values;
            if (visite != null) {
                date.setText(visite.get(position).getDate().split("T")[0]);
                String final_rdv = visite.get(position).getRdv().toString().trim().startsWith("1") ? "oui" : "non";
                rdv.setText(final_rdv);
                heurearrive.setText(visite.get(position).getHeureArrive());
                heuredepart.setText(visite.get(position).getHeureDepart());
               // cabinet.setText("cbinet");
                loadInformation(medecin, cabinet, position);
            }
        }

        if(values.get(position) instanceof VisiteLocal){
            visite_bdd=(ArrayList<VisiteLocal>) values;
            if (visite_bdd != null) {
                date.setText(visite_bdd.get(position).date);
                String final_rdv = String.valueOf(visite_bdd.get(position).rdv).trim().startsWith("1") ? "oui" : "non";
                rdv.setText(final_rdv);
                heurearrive.setText(visite_bdd.get(position).heure_arrive);
                heuredepart.setText(visite_bdd.get(position).heure_depart);
                medecin.setText("non disponible");
                cabinet.setText("non disponible");

            }
        }


        return convertView;
    }

    private void loadInformation(final TextView medecin, final TextView cabinet , final int position) {
        Callback<List<Medecin>> serviceCallback = new Callback<List<Medecin>> () {
            @Override
            public void onResponse(Call<List<Medecin>> call, Response<List<Medecin>> response) {
                medecin.setText(response.body().get(0).getNom());
                loadCabinetId(cabinet, response.body().get(0).getIdCabinet());
            }

            @Override
            public void onFailure(Call<List<Medecin>> call, Throwable t) {
                Log.e("SynchroManager", "Load Medias", t);
            }
        };

        ApiService service = ServiceManager.createService(ApiService.class);
        Call<List<Medecin>>  call = service.getMedecinById(visite.get(position).getIdMedecin());
        call.enqueue(serviceCallback);

    }

    private void loadCabinetId(final TextView cabinet, int id) {
        Callback<List<Cabinet>> serviceCallback = new Callback<List<Cabinet>> () {
            @Override
            public void onResponse(Call<List<Cabinet>> call, Response<List<Cabinet>> response) {
                cabinet.setText(response.body().get(0).getVille() + ", " + response.body().get(0).getAdresse());
            }

            @Override
            public void onFailure(Call<List<Cabinet>> call, Throwable t) {
                Log.e("SynchroManager", "Load Medias", t);
            }
        };

        ApiService service = ServiceManager.createService(ApiService.class);
        Call<List<Cabinet>>  call = service.getCabinetById(id);
        call.enqueue(serviceCallback);

    }
}

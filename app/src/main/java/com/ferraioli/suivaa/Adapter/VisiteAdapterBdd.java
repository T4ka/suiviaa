package com.ferraioli.suivaa.Adapter;

/**
 * Created by Ferraioli on 25/04/2018.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ferraioli.suivaa.ModelBDD.VisiteLocal;
import com.ferraioli.suivaa.R;

import java.util.ArrayList;


public class VisiteAdapterBdd extends ArrayAdapter<VisiteLocal> {
    private final Context context;
    private final ArrayList<VisiteLocal> values;

    public VisiteAdapterBdd(Context context, ArrayList<VisiteLocal> values) {
        super(context, R.layout.visites_list, values);
        this.context = context;
        this.values = values;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.visites_list, parent, false);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.list_item_show_title);
        VisiteLocal visite = values.get(position);


        return convertView;
    }
}
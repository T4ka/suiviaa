package com.ferraioli.suivaa.ModelBDD;

/**
 * Created by Ferraioli on 25/04/2018.
 */

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


@Entity(tableName = "VisiteLocal")
public class VisiteLocal {
    @PrimaryKey(autoGenerate = true)
    public @NonNull
    int id;

    public @NonNull
    String date;

    public @NonNull
    String rdv;

    public @NonNull
    String heure_arrive;

    public @NonNull
    String heure_depart;

    public @NonNull
    int id_medecin;

    public @NonNull
    boolean est_en_api = false;

}
